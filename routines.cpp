
/*  This is a program to compute the pair-correlation function of a 
 *  monoatomic system on a shared-memory multi-core CPU, using OpenMP
 * 
 *  Copyright (C) 2020  Arunkumar Bupathy (arunbupathy@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

# include <cstdlib>
# include <cmath>
# include <omp.h>
# include <string>

# define EXT extern
# include "params.hpp"
# undef EXT

////////////////////////////////////////////////////////////////////////////////

void get_pair_corr_unbalanced(unsigned long long * corr, double * xx, double * yy, double * zz, double Lx, double Ly, double Lz, unsigned N, double BinWid, unsigned nthreads)
{
    unsigned nBins = ceil(sqrt(Lx * Lx + Ly * Ly + Lz * Lz) / 2.0 / BinWid);
    
    /* The most straight forward pair correlation code
     * now distributed across multiple threads
     * The reduction() clause is necessary because corr[]
     * is common to all the threads */
    # pragma omp parallel for reduction(+:corr[:nBins]) num_threads(nthreads)
    for(unsigned i = 0; i < N - 1; ++i)
    {
        
        for(unsigned j = i + 1; j < N; j++)
        {
            double diffX = xx[j] - xx[i];
            double diffY = yy[j] - yy[i];
            double diffZ = zz[j] - zz[i];
            
            // pbc
            double rX = diffX - Lx * lround(diffX / Lx);
            double rY = diffY - Ly * lround(diffY / Ly);
            double rZ = diffZ - Lz * lround(diffZ / Lz);
            
            double rsq = rX * rX + rY * rY + rZ * rZ;
            double rij_scaled = sqrt(rsq) / BinWid;
            
            unsigned icntr = lround(rij_scaled);
            
            // We don't check if rij is within bounds, as corr[] is sufficiently large
            corr[icntr]++;
        }
        
    } // end of parallelized for loop
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void get_pair_corr_static1(unsigned long long * corr, double * xx, double * yy, double * zz, double Lx, double Ly, double Lz, unsigned N, double BinWid, unsigned nthreads)
{
    unsigned nBins = ceil(sqrt(Lx * Lx + Ly * Ly + Lz * Lz) / 2.0 / BinWid);
    
    /* schedule(static,1) distributes the loop iterations
     * such that the n threads process n consecutive iterations
     * at a time, sequentially */
    # pragma omp parallel for schedule(static,1) reduction(+:corr[:nBins]) num_threads(nthreads)
    for(unsigned i = 0; i < N - 1; ++i)
    {
        
        for(unsigned j = i + 1; j < N; j++)
        {
            double diffX = xx[j] - xx[i];
            double diffY = yy[j] - yy[i];
            double diffZ = zz[j] - zz[i];
            
            // pbc
            double rX = diffX - Lx * lround(diffX / Lx);
            double rY = diffY - Ly * lround(diffY / Ly);
            double rZ = diffZ - Lz * lround(diffZ / Lz);
            
            double rsq = rX * rX + rY * rY + rZ * rZ;
            double rij_scaled = sqrt(rsq) / BinWid;
            
            unsigned icntr = lround(rij_scaled);
            
            corr[icntr]++;
        }
        
    } // end of parallelized for loop
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void get_pair_corr_dynamic64(unsigned long long * corr, double * xx, double * yy, double * zz, double Lx, double Ly, double Lz, unsigned N, double BinWid, unsigned nthreads)
{
    unsigned nBins = ceil(sqrt(Lx * Lx + Ly * Ly + Lz * Lz) / 2.0 / BinWid);
    
    /* schedule(dynamic,64) distributes the loop iterations
     * such that the each thread processes chunks of 64 consecutive
     * iterations at a time, but the distribution of these chunks can
     * be dynamic, i.e., any idle thread can get the next chunk */
    # pragma omp parallel for schedule(dynamic,64) reduction(+:corr[:nBins]) num_threads(nthreads)
    for(unsigned i = 0; i < N - 1; ++i)
    {
        
        for(unsigned j = i + 1; j < N; j++)
        {
            double diffX = xx[j] - xx[i];
            double diffY = yy[j] - yy[i];
            double diffZ = zz[j] - zz[i];
            
            // pbc
            double rX = diffX - Lx * lround(diffX / Lx);
            double rY = diffY - Ly * lround(diffY / Ly);
            double rZ = diffZ - Lz * lround(diffZ / Lz);
            
            double rsq = rX * rX + rY * rY + rZ * rZ;
            double rij_scaled = sqrt(rsq) / BinWid;
            
            unsigned icntr = lround(rij_scaled);
            
            corr[icntr]++;
        }
        
    } // end of parallelized for loop
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void get_pair_corr_tiled(unsigned long long * corr, double * xx, double * yy, double * zz, double Lx, double Ly, double Lz, unsigned N, double BinWid, unsigned nthreads, unsigned tilelen)
{
    unsigned numTiles = ceil(((double) N) / ((double) tilelen));
    unsigned nBins = ceil(sqrt(Lx * Lx + Ly * Ly + Lz * Lz) / 2.0 / BinWid);
    
    /* here we break down the N x N pairs into tilelen x tilelen tiles
     * and read the corresponding input data into local arrays
     * as a result, with only O(2 x tilelen) memory read operations 
     * we can perform O(tilelen x tilelen) arithmetic operations.
     * 
     * memory bandwidth can be a serious limiting factor
     * this is a pretty common trick used in GPU programming
     * to reduce bandwidth utilization. */
    
    # pragma omp parallel for schedule(dynamic,1) reduction(+:corr[:nBins]) num_threads(nthreads)
    for(unsigned tile = 0; tile < numTiles * numTiles; ++tile)
    {
        // pick a tile, and find its x and y co-ordinate
        unsigned itile = tile % numTiles;
        unsigned jtile = tile / numTiles;
        
        unsigned ibase = itile * tilelen; // starting index of the outer loop for this tile
        unsigned jbase = jtile * tilelen; // starting index of the inner loop for this tile
        
        // only proceed if the tile is on the diagonal or the upper triangle
        if(ibase >= jbase)
        {
            double local_x[2][tilelen];
            double local_y[2][tilelen];
            double local_z[2][tilelen];
            
            unsigned baseij[2] = {ibase, jbase};
            // this will tell whether we should read the coordinates of the p'les
            // corresponding to the outer loop (0) or the inner loop (1)
            
            // now, load the input data into local arrays
            // there is a good chance they are chached for performance
            for(unsigned j0 = 0; j0 < 2; ++j0)
            {
                // local[0][] is to hold the p'le co-ordinates corresp. to the outer loop
                // local[1][] is to hold the p'le co-ordinates corresp. to the inner loop
                
                for(unsigned i0 = 0; i0 < tilelen; ++i0)
                {
                    // this is the index of the p'le whose co-ordinates are to be loaded
                    unsigned index = baseij[j0] + i0;
                    
                    if(index < N) // necessary, as N may not be a multiple of tilelen
                    {
                        local_x[j0][i0] = xx[index];
                        local_y[j0][i0] = yy[index];
                        local_z[j0][i0] = zz[index];
                    }
                }
            }
            
            // now use the locally stored co-ordinates to compute the pair correlations
            for(unsigned i0 = 0; i0 < tilelen; ++i0)
            {
                // this is the particle index corresponding to the outer loop
                unsigned ple_i = ibase + i0;
                
                if(ple_i < N) // necessary, as N may not be a multiple of tilelen
                {
                    
                    for(unsigned j0 = 0; j0 < tilelen; ++j0)
                    {
                        // this is the particle index corresponding to the inner loop
                        unsigned ple_j = jbase + j0;
                        
                        if(ple_j < ple_i) // make sure we're not double counting
                        {
                            double diffX = local_x[0][i0] - local_x[1][j0];
                            double diffY = local_y[0][i0] - local_y[1][j0];
                            double diffZ = local_z[0][i0] - local_z[1][j0];
                            
                            // pbc
                            double rX = diffX - Lx * lround(diffX / Lx);
                            double rY = diffY - Ly * lround(diffY / Ly);
                            double rZ = diffZ - Lz * lround(diffZ / Lz);
                            
                            double rsq = rX * rX + rY * rY + rZ * rZ;
                            
                            double rij_scaled = sqrt(rsq) / BinWid;
                            
                            unsigned icntr = lround(rij_scaled);
                            
                            corr[icntr]++;
                        }
                    }
                }
            }
        }
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void get_pair_corr_tiled_staged(unsigned long long * corr, double * xx, double * yy, double * zz, double Lx, double Ly, double Lz, unsigned N, double BinWid, unsigned nthreads, unsigned tilelen)
{
    unsigned numTiles = ceil(((double) N) / ((double) tilelen));
    unsigned nBins = ceil(sqrt(Lx * Lx + Ly * Ly + Lz * Lz) / 2.0 / BinWid);
    
    /* Same as previous, but the accumulation of the histogram is moved to a separate loop */
    # pragma omp parallel for schedule(dynamic,1) reduction(+:corr[:nBins]) num_threads(nthreads)
    for(unsigned tile = 0; tile < numTiles * numTiles; ++tile)
    {
        unsigned itile = tile % numTiles;
        unsigned jtile = tile / numTiles;
        unsigned ibase = itile * tilelen;
        unsigned jbase = jtile * tilelen;
        
        if(ibase >= jbase)
        {
            double local_x[2][tilelen];
            double local_y[2][tilelen];
            double local_z[2][tilelen];
            
            unsigned baseij[2] = {ibase, jbase};
            for(unsigned j0 = 0; j0 < 2; ++j0)
            {
                for(unsigned i0 = 0; i0 < tilelen; ++i0)
                {
                    unsigned index = baseij[j0] + i0;
                    if(index < N)
                    {
                        local_x[j0][i0] = xx[index];
                        local_y[j0][i0] = yy[index];
                        local_z[j0][i0] = zz[index];
                    }
                }
            }
            
            unsigned icntrs[tilelen];
            for(unsigned i0 = 0; i0 < tilelen; ++i0)
            {
                unsigned ple_i = ibase + i0;
                if(ple_i < N)
                {
                    // in the loop below, we only collect the indices (icntrs[])
                    // of the histogram bins that need to be updated
                    // as a result, this loop's control flow is straight forward
                    // leading to better CPU pipeline and cache utilization
                    for(unsigned j0 = 0; j0 < tilelen; ++j0)
                    {
                        double diffX = local_x[0][i0] - local_x[1][j0];
                        double diffY = local_y[0][i0] - local_y[1][j0];
                        double diffZ = local_z[0][i0] - local_z[1][j0];
                        
                        // pbc
                        double rX = diffX - Lx * round(diffX / Lx);
                        double rY = diffY - Ly * round(diffY / Ly);
                        double rZ = diffZ - Lz * round(diffZ / Lz);
                        
                        double rsq = rX * rX + rY * rY + rZ * rZ;
                        
                        double rij_scaled = sqrt(rsq) / BinWid;
                        
                        icntrs[j0] = round(rij_scaled);
                    }
                    
                    // now we do the actual histogramming
                    for(unsigned j0 = 0; j0 < tilelen; ++j0)
                    {
                        unsigned ple_j = jbase + j0;
                        if(ple_j < ple_i)
                            corr[icntrs[j0]]++;
                    }
                }
            }
        }
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
