CXX = g++ -Wall -fopenmp
OPFLAGS = -O2 -march=native
LDFLAGS = -lm

all: omp-pair-corr
	rm -f helper.o routines_fast.o routines.o main.o

omp-pair-corr: helper.o routines_fast.o routines.o main.o
	$(CXX) $? $(LDFLAGS) -o $@

helper.o: helper.cpp
	$(CXX) $(OPFLAGS) $? -c

routines_fast.o: routines_fast.cpp
	$(CXX) $(OPFLAGS) -fno-math-errno -fno-trapping-math -fopt-info-vec-optimized $? -c 

routines.o: routines.cpp
	$(CXX) $(OPFLAGS) $? -c

main.o: main.cpp
	$(CXX) $(OPFLAGS) $? -c
