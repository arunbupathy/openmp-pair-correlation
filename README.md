# OpenMP Pair Correlation

## A standalone program that computes the pair correlation function of a 3D mono-atomic system using OpenMP on a multi-core CPU machine.

Multiple versions of the pair-correlation function are provided (routines_fast.cpp and routines.cpp). They differ in the way in which they decimate the computations into smaller parallel chunks.

The "unbalanced" version is not very efficient. Because we discard some particle pairs (*ij* is the same as *ji*), the default stride used to distribute the loop among threads ends up creating a variable amount of work for each thread.

The `schedule` clause in the `parallel for` pragma defines which threads are assigned which loop iterations, and helps to balance out the computations between the threads. Static scheduling is good for smaller data sets, while dynamic is better for larger data sets.

The "tiled" version divides the work into M x M sized blocks (default M = 256), each of which is processed by a single thread. By caching the input values for each M x M tile, data movement to and from main memory is reduced leading to slightly better performance.

The "staged" version shifts the accumulation of the histogram and the associated conditional checks into a separate stage. The performance improvement is probably due to the work load being predictable, improving cache hits and avoiding pipeline flushes.

The "nxp" versions are essentially the same as tiled-staged, except they are compiled with `-fno-math-errno` and `-fno-trapping-math` flags. While they violate the IEEE specifications for floating point math, [they do not seem to fundamentally change the accuracy of the results](https://stackoverflow.com/questions/57673825/how-to-force-gcc-to-assume-that-a-floating-point-expression-is-non-negative). This alone gives about 1.5x improvement in speed.

Lastly, the "SIMD" version uses vectorization to achieve almost 2x speed up. Note however that it does not conform to the IEEE floating point standard, as the flags described above are necessary to enable vectorization.

### The input file should be formatted as shown below (see provided input.dat for a sample):

    box_start_x      box_end_x
    box_start_y      box_end_y
    box_start_z      box_end_z
    
    x1   y1   z1
    ...
    ...
    ...
    xN   yN   zN
   
### Run the program as follows:

`./omp-pair-corr -in <input_file> -out <output_file> -binwidth <bw> -nthreads <nt>`
