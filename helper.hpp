
int hashit(std::string);

int get_params(int, char**);

void read_file(double *, double *, double *, std::string);

void store_pair_corr(unsigned long long *, double, double, double, unsigned, double, std::string);

timespec time_diff(timespec, timespec);

void current_utc_time(struct timespec *);
