
/*  This is a program to compute the pair-correlation function of a 
 *  monoatomic system on a shared-memory multi-core CPU, using OpenMP
 * 
 *  Copyright (C) 2020  Arunkumar Bupathy (arunbupathy@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

# include <cstdlib>
# include <cmath>
# include <omp.h>
# include <string>

# define EXT extern
# include "params.hpp"
# undef EXT
 
////////////////////////////////////////////////////////////////////////////////

/* Same as get_pair_corr_tiled_staged() found in routines.cpp, except we compile this file with
 * -fno-math-errno and -fno-trapping-math which seem to turn off some unnecessary checks */

void get_pair_corr_tiled_staged_nxp(unsigned long long * corr, double * xx, double * yy, double * zz, double Lx, double Ly, double Lz, unsigned N, double BinWid, unsigned nthreads, unsigned tilelen)
{
    unsigned numTiles = ceil(((double) N) / ((double) tilelen));
    unsigned nBins = ceil(sqrt(Lx * Lx + Ly * Ly + Lz * Lz) / 2.0 / BinWid);
    
    # pragma omp parallel for schedule(dynamic,1) reduction(+:corr[:nBins]) num_threads(nthreads)
    for(unsigned tile = 0; tile < numTiles * numTiles; ++tile)
    {
        unsigned itile = tile % numTiles;
        unsigned jtile = tile / numTiles;
        unsigned ibase = itile * tilelen;
        unsigned jbase = jtile * tilelen;
        
        if(ibase >= jbase)
        {
            double local_x[2][tilelen];
            double local_y[2][tilelen];
            double local_z[2][tilelen];
            
            unsigned baseij[2] = {ibase, jbase};
            for(unsigned j0 = 0; j0 < 2; ++j0)
            {
                for(unsigned i0 = 0; i0 < tilelen; ++i0)
                {
                    unsigned index = baseij[j0] + i0;
                    if(index < N)
                    {
                        local_x[j0][i0] = xx[index];
                        local_y[j0][i0] = yy[index];
                        local_z[j0][i0] = zz[index];
                    }
                }
            }
            
            unsigned icntrs[tilelen];
            for(unsigned i0 = 0; i0 < tilelen; ++i0)
            {
                unsigned ple_i = ibase + i0;
                if(ple_i < N)
                {
                    for(unsigned j0 = 0; j0 < tilelen; ++j0)
                    {
                        double diffX = local_x[0][i0] - local_x[1][j0];
                        double diffY = local_y[0][i0] - local_y[1][j0];
                        double diffZ = local_z[0][i0] - local_z[1][j0];
                        
                        // pbc
                        double rX = diffX - Lx * round(diffX / Lx);
                        double rY = diffY - Ly * round(diffY / Ly);
                        double rZ = diffZ - Lz * round(diffZ / Lz);
                        
                        double rsq = rX * rX + rY * rY + rZ * rZ;
                        
                        double rij_scaled = sqrt(rsq) / BinWid;
                        
                        icntrs[j0] = round(rij_scaled);
                    }
                    
                    for(unsigned j0 = 0; j0 < tilelen; ++j0)
                    {
                        unsigned ple_j = jbase + j0;
                        if(ple_j < ple_i)
                            corr[icntrs[j0]]++;
                    }
                }
            }
        }
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

/* Same as previous, but the innermost loop is now vectorized */

void get_pair_corr_tiled_staged_nxp_simd(unsigned long long * corr, double * xx, double * yy, double * zz, double Lx, double Ly, double Lz, unsigned N, double BinWid, unsigned nthreads, unsigned tilelen)
{
    unsigned numTiles = ceil(((double) N) / ((double) tilelen));
    unsigned nBins = ceil(sqrt(Lx * Lx + Ly * Ly + Lz * Lz) / 2.0 / BinWid);
    
    # pragma omp parallel for schedule(dynamic,1) reduction(+:corr[:nBins]) num_threads(nthreads)
    for(unsigned tile = 0; tile < numTiles * numTiles; ++tile)
    {
        unsigned itile = tile % numTiles;
        unsigned jtile = tile / numTiles;
        unsigned ibase = itile * tilelen;
        unsigned jbase = jtile * tilelen;
        
        if(ibase >= jbase)
        {
            double local_x[2][tilelen];
            double local_y[2][tilelen];
            double local_z[2][tilelen];
            
            unsigned baseij[2] = {ibase, jbase};
            for(unsigned j0 = 0; j0 < 2; ++j0)
            {
                for(unsigned i0 = 0; i0 < tilelen; ++i0)
                {
                    unsigned index = baseij[j0] + i0;
                    if(index < N)
                    {
                        local_x[j0][i0] = xx[index];
                        local_y[j0][i0] = yy[index];
                        local_z[j0][i0] = zz[index];
                    }
                }
            }
            
            unsigned icntrs[tilelen];
            for(unsigned i0 = 0; i0 < tilelen; ++i0)
            {
                unsigned ple_i = ibase + i0;
                if(ple_i < N)
                {
                    // this pragma instructs the compiler to use the CPU's
                    // vector units to compute multiple iterations simultaneously
                    // how many iterations depends on the width of the simd unit
                    # pragma omp simd
                    for(unsigned j0 = 0; j0 < tilelen; ++j0)
                    {
                        double diffX = local_x[0][i0] - local_x[1][j0];
                        double diffY = local_y[0][i0] - local_y[1][j0];
                        double diffZ = local_z[0][i0] - local_z[1][j0];
                        
                        // pbc
                        double rX = diffX - Lx * round(diffX / Lx);
                        double rY = diffY - Ly * round(diffY / Ly);
                        double rZ = diffZ - Lz * round(diffZ / Lz);
                        
                        double rsq = rX * rX + rY * rY + rZ * rZ;
                        
                        double rij_scaled = sqrt(rsq) / BinWid;
                        
                        icntrs[j0] = round(rij_scaled);
                    }
                    
                    for(unsigned j0 = 0; j0 < tilelen; ++j0)
                    {
                        unsigned ple_j = jbase + j0;
                        if(ple_j < ple_i)
                            corr[icntrs[j0]]++;
                    }
                }
            }
        }
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////
