
void get_pair_corr_tiled_staged_nxp_simd(unsigned long long *, double *, double *, double *, double, double, double, unsigned, double, unsigned, unsigned);

void get_pair_corr_tiled_staged_nxp(unsigned long long *, double *, double *, double *, double, double, double, unsigned, double, unsigned, unsigned);

void get_pair_corr_tiled_staged(unsigned long long *, double *, double *, double *, double, double, double, unsigned, double, unsigned, unsigned);

void get_pair_corr_tiled(unsigned long long *, double *, double *, double *, double, double, double, unsigned, double, unsigned, unsigned);

void get_pair_corr_dynamic64(unsigned long long *, double *, double *, double *, double, double, double, unsigned, double, unsigned);

void get_pair_corr_static1(unsigned long long *, double *, double *, double *, double, double, double, unsigned, double, unsigned);

void get_pair_corr_unbalanced(unsigned long long *, double *, double *, double *, double, double, double, unsigned, double, unsigned);
