
/*  A simple g(r) code to test the effect of compiler optimizations
 * 
 * Author: Arunkumar Bupathy
 * email: arunbupathy@gmail.com
 * 
 * This software is distributed under the GNU GPLv3
 * See www.gnu.org for the licence terms */

////////////////////////////////////////////////////////////////////////////////

# include <iostream>
# include <cstdlib>
# include <cmath>

# define EXT ;
# include "params.hpp"
# undef EXT
# include "helper.hpp"
# include "routines.hpp"

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
    g_BinWid = 0.02; // a reasonable default
    g_numthreads = 1; // run single threaded by default
    unsigned tilelen = 256; // the default tile length is 256
    
    int state = get_params(argc, argv); // get input and output file names
    if(state != 0) return 1; // means the infile could not be read
    
    double * xx = new double [g_N] (); // x coord
    double * yy = new double [g_N] (); // y coord
    double * zz = new double [g_N] (); // z coord
    
    read_file(xx, yy, zz, g_infile); // read the co-ordinates
    
    unsigned long long * corr = new unsigned long long [g_NBins] (); // pair correlation function
    
    struct timespec tstart, tend;
    current_utc_time(&tstart);
    
    // this is the fastest version (see routines_fast.cpp and routines.cpp for other versions)
    get_pair_corr_tiled_staged_nxp_simd(corr, xx, yy, zz, g_Lx, g_Ly, g_Lz, g_N, g_BinWid, g_numthreads, tilelen);
    
    current_utc_time(&tend);
    struct timespec tdiff = time_diff(tstart, tend);
    
    std::cout << "Time taken for the computations: " << ((tdiff.tv_sec * 1000000000 + tdiff.tv_nsec) / 1000000000.0) << "s." << std::endl;
    
    delete[] xx; // free the memory
    delete[] yy;
    delete[] zz;
    
    store_pair_corr(corr, g_Lx, g_Ly, g_Lz, g_N, g_BinWid, g_outfile);
    
    delete[] corr;
    
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
